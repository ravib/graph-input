var topics = ['Classical Mechanics','Quantum Mechanics','Electromagnetism','Thermodynamics and Stastical Mechanics','Relativity' 
    /*,'Condensed Matter','Astrophysics','Optics','Acoustics','Nuclear Physics'*/];

var categories = ["experiment","thought-experiment","device","inference","concept","theory","law","quantity","observation","natural-phenomenon","paradox"];

var subtopics = {
    "classical-mechanics": ["Newton's laws of motion","Lagrangian Mechanics","Hamiltonian Mechanics","Kinematics","Statics","Dynamics","Chaos Theory",
        "Acoustics","Fluid Dynamics","Continuum Mechanics"],
    "quantum-mechanics" : ["Path Integral Formulation","Scattering Theory","Schrödinger Equation","Quantum Field Theory","Quantum Statistical Mechanics"],
    "electromagnetism": ["Electrostatics","Electrodynamics","Electricity","Magnetism","Magnetostatics","Maxwell's equations","Optics"],
    "thermodynamics-and-stastical-mechanics": ["Heat Engine","Kinetic Theory"],
    "relativity": ["Special Relativity","General Relativity","Einstein Field Equations"]
}

var labels = ["implies","derives","supersedes","leads-to","approximates","contradicts"];
