function str_clean(s)
{
  s = s.trim();
  s = s.toLowerCase();
  s = s.replaceAll(' ', '-');
  return s;
}
//TODO create function that could be used with forEach
//TODO checkbox and label get seperated when window is resized
function create_checkboxes(parent_obj, name, item_list, hidden, div_name=name)
{
  var d = document.createElement("div");
  d.id = div_name;
  if(hidden)
    d.style.display = "none";
  for(let t of item_list)
  {
    //console.log(t.v);
    var cb1 = document.createElement("input");
    var label1 = document.createElement("label");
    
    cb1.type = "checkbox";
    cb1.name = name;
    var x = str_clean(t);
    cb1.id = x;
    cb1.value = x;
    //d.appendChild(cb1);
    label1.appendChild(cb1);
    
	//label1.setAttribute("for", "classical");
    //label1.htmlFor = x;
    label1.innerHTML += t;
	//label1.style.whiteSpace = "nowrap";
    d.appendChild(label1);
  }
  parent_obj.appendChild(d);
  //parent_obj.appendChild(document.createElement("br"));
}

function create_header(lvl, parent_obj, title)
{
  var h4_obj = document.createElement("h"+lvl.toString());
  h4_obj.id = str_clean(title+"_h");
  h4_obj.innerHTML = title;
  parent_obj.appendChild(h4_obj);
}


function create_node_form()
{
  var br = document.createElement("br");
  
  var form = document.createElement("form");
  form.method = "POST";
  form.id = "node-input";
  
  //topic
  var d = document.createElement("div");
  d.id = "topic_div";
  
  //create_header(4, d, "Topic");
  var field_set = document.createElement("fieldset");
  field_set.id = "topic_field";
  var legend = document.createElement("legend");
  legend.innerHTML = "Topic(s):";
  field_set.appendChild(legend);
  
  create_checkboxes(field_set, "topic", topics, false);
  
  d.appendChild(field_set);
  form.appendChild(d);
  
  //title
  
  d = document.createElement("div");
  d.id = "title_div";
  
  //create_header(4, d, "Title");
  
  var title_tb = document.createElement("input");
  title_tb.type = "text";
  title_tb.id = "title"
  title_tb.name = "title"
  title_tb.required = true;
  title_tb.pattern = "[a-zA-Z0-9 _&-]{4,40}";
  title_tb.autocomplete = "off";
  //title_tb.maxlength = 40;
  //title_tb.minlength = 4;
  title_tb.size = 40;
  
  var la = document.createElement("label");
  la.htmlFor = "title";
  la.innerHTML = "Title:";
  
  d.appendChild(la);
  d.appendChild(title_tb);
  
  form.appendChild(d);
  
  //category
  
  d = document.createElement("div");
  d.id = "title_div";
  //create_header(4, d, "Category");
  field_set = document.createElement("fieldset");
  field_set.id = "category_field";
  legend = document.createElement("legend");
  legend.innerHTML = "Category(s):";
  field_set.appendChild(legend);
  
  create_checkboxes(field_set, "category", categories, false);
  
  d.appendChild(field_set);
  form.appendChild(d);
  
  //description
  d = document.createElement("div");
  d.id = "description_div";
  
  //create_header(4, d, "Description");
  
  var desc_tb = document.createElement("textarea");
  desc_tb.id = "description";
  desc_tb.name = "description";
  desc_tb.cols = 50;
  desc_tb.rows = 4;
  
  la = document.createElement("label");
  la.htmlFor = "description";
  la.innerHTML = "Description:";
  
  d.appendChild(la);
  d.appendChild(desc_tb);
  
  form.appendChild(d);
  
  //subtopic
  d = document.createElement("div");
  d.id = "subtopic_div";
  d.style.display = "none";
  
  //create_header(4, d, "Sub-topic");
  field_set = document.createElement("fieldset");
  field_set.id = "subtopic_field";
  legend = document.createElement("legend");
  legend.innerHTML = "Sub-topic(s):";
  field_set.appendChild(legend);
  
  for(var t in subtopics)
  {
    create_checkboxes(field_set, "subtopic", subtopics[t], true, t+"_div");
  }
  
  d.appendChild(field_set);
  form.appendChild(d);
  
  form.appendChild(br.cloneNode());
  
  var s = document.createElement("input"); 
  s.setAttribute("type", "submit"); 
  s.setAttribute("value", "Submit"); 
  form.appendChild(s); 
  form.appendChild(br.cloneNode()); 
  
  var form_type = document.createElement("input");
  form_type.type = "hidden";
  form_type.id = "type"
  form_type.name = "type"
  form_type.value = "node";
  form.appendChild(form_type);
  
  return form;
}

function create_edge_form()
{
  var br = document.createElement("br");
  
  var form = document.createElement("form");
  form.method = "POST";
  form.id = "edge-input";
  form.autocomplete = "off";
  
  //src
  create_header(4, form, "Source Node");
  
  var src_div = document.createElement("div");
  src_div.className = "autocomplete";
  src_div.style.width = "300px";
  
  var src_tb = document.createElement("input");
  src_tb.type = "text";
  src_tb.id = "_from"
  src_tb.name = "_from"
  src_tb.required = true;
  src_tb.pattern = ".{4,40}";
  //src_tb.autocomplete = "off";
  //title_tb.maxlength = 40;
  //title_tb.minlength = 4;
  src_tb.size = 40;
  
  src_div.appendChild(src_tb);
  form.appendChild(src_div);
  
  //label
  create_header(4, form, "Label");
  
  var label_div = document.createElement("div");
  label_div.className = "autocomplete";
  label_div.style.width = "300px";
  
  var label_tb = document.createElement("input");
  label_tb.type = "text";
  label_tb.id = "label"
  label_tb.name = "label"
  label_tb.required = true;
  label_tb.pattern = ".{4,40}";
  //label_tb.autocomplete = "off";
  //title_tb.maxlength = 40;
  //title_tb.minlength = 4;
  label_tb.size = 40;
  
  label_div.appendChild(label_tb);
  form.appendChild(label_div);
  
  //dest
  create_header(4, form, "Destination Node");
  
  var dest_div = document.createElement("div");
  dest_div.className = "autocomplete";
  dest_div.style.width = "300px";
  
  var dest_tb = document.createElement("input");
  dest_tb.type = "text";
  dest_tb.id = "_to"
  dest_tb.name = "_to"
  dest_tb.required = true;
  dest_tb.pattern = ".{4,40}";
  //dest_tb.autocomplete = "off";
  //title_tb.maxlength = 40;
  //title_tb.minlength = 4;
  dest_tb.size = 40;
  
  dest_div.appendChild(dest_tb);
  form.appendChild(dest_div);
  
  form.appendChild(br.cloneNode());
  form.appendChild(br.cloneNode());
  
  var s = document.createElement("input"); 
  s.setAttribute("type", "submit"); 
  s.setAttribute("value", "Submit"); 
  form.appendChild(s); 
  form.appendChild(br.cloneNode());
  
  var form_type = document.createElement("input");
  form_type.type = "hidden";
  form_type.id = "type"
  form_type.name = "type"
  form_type.value = "edge";
  form.appendChild(form_type);
  
  return form;
}
