from flask import Flask, request, render_template, send_from_directory

"""
create directories "../data/nodes/", "../data/edges/"
run 'pip install Flask sh ArangoClient'
run 'export FLASK_APP=server.py'
run 'flask run'
"""

save_to_arango = True
save_to_document = True
commit_to_git = True

get_node_list_onsubmit = True

#TODO these are not used in git commit
nodes_doc_folder = "../data/nodes/"
edges_doc_folder = "../data/edges/"

db_name = 'physics'
node_collection_name = 'nodes'
edge_collection_name = 'links'
usr_name = 'bhoot'
usr_pwd = 'aqz'
arango_url = 'http://localhost:8529'

######################################
# code start
######################################

key_id = "_key"
type_id = "type"
type_node_s = "node"
type_edge_s = "edge"

#node form
topic_id = "topic"
title_id = "title"
desc_id = "description"
title_id = "title"

#edge form
src_node_id = "_from"
dest_node_id = "_to"
label_id = "label"

if save_to_arango:
    from arango import ArangoClient
if save_to_document:
    import json
    from datetime import datetime
# do not commit to git if documents aren't being made
else:
    commit_to_git = False
if commit_to_git:
    import sh


app = Flask(__name__, static_url_path='')

node_collection = 0
edge_collection = 0
db = 0
node_list = []

def init_db():
    global db
    global node_collection
    global edge_collection
    
    client = ArangoClient(hosts=arango_url)
    # Connect to "_system" database as root user.
    # This returns an API wrapper for "_system" database.
    sys_db = client.db('_system', username='root', password='arangopass')

    # Create a new database named "physics" if it does not exist.
    if not sys_db.has_database(db_name):
        #sys_db.create_database(db_name)
        print("E:db not found")
        return
    
    db = client.db(db_name, username=usr_name, password=usr_pwd)
    
    #collection init
    if db.has_collection(node_collection_name):
        node_collection = db.collection(node_collection_name)
    else:
        print("W:collection not found and created: "+node_collection_name)
        node_collection = db.create_collection(node_collection_name)
    if db.has_collection(edge_collection_name):
        edge_collection = db.collection(edge_collection_name)
    else:
        print("W:collection not found and created: "+edge_collection_name)
        edge_collection = db.create_collection(edge_collection_name)

def get_node_list():
    global node_list
    # Execute an AQL query. This returns a result cursor.
    cursor = db.aql.execute('FOR doc IN ' + node_collection_name + ' RETURN doc')

    # Iterate through the cursor to retrieve the documents.
    node_list = [str(document[key_id]) for document in cursor]

if save_to_arango:
    init_db()
    get_node_list()

@app.route('/', methods=['GET', 'POST'])
def root():
    #process data recieved from POST request
    if request.method == 'POST':
        data = request.form.to_dict(False)
        is_node = data[type_id][0] == type_node_s
        print(data)
        data.pop(type_id, None)
        print("V:recieved data")
        print(data)
        print(is_node)
        if is_node:
            data[title_id] = data[title_id][0]
            data[desc_id] = data[desc_id][0]
        else:
            data[src_node_id] = data[src_node_id][0]
            data[label_id] = data[label_id][0]
            data[dest_node_id] = data[dest_node_id][0]
            
            git_m = data[src_node_id]+' '+data[label_id]+' '+data[dest_node_id]
            
            data[src_node_id] = node_collection_name+'/'+data[src_node_id]
            data[dest_node_id] = node_collection_name+'/'+data[dest_node_id]
        if save_to_arango:
            store_in_db(data, is_node)
            if is_node:
                if get_node_list_onsubmit:
                    get_node_list()
                else:
                    node_list.append(reformat_str(data[title_id]))
                print(node_list)
        if save_to_document:
            create_document(data, is_node)
            if commit_to_git:
                if is_node:
                    git_commit_doc(reformat_str(data[title_id]), is_node)
                else:
                    git_commit_doc(git_m, is_node)
        #show the form again
        return render_template('index.html', node_list_py = node_list)
    #render the form
    if request.method == 'GET':
        return render_template('index.html', node_list_py = node_list)

#used to create filename and key
def reformat_str(s):
    s = s.lower()
    s = ' '.join(s.split())
    s = s.replace(' ', '-')
    return s

def store_in_db(d, is_n):
    #add data to collection
    if is_n:
        d[key_id] = reformat_str(d[title_id])
        node_collection.insert(d)
    else:
        edge_collection.insert(d)

def create_document(d, is_n):
    if is_n:
        s = reformat_str(d[title_id])
        with open(nodes_doc_folder+s, 'w') as f: 
            f.write(json.dumps(d))
        print("V:" + s + " file created")
    else:
        now = datetime.now()
        date_time = now.strftime("%d%m%Y_%H%M%S")
        with open(edges_doc_folder+date_time, 'w') as f: 
            f.write(json.dumps(d))
        print("V: edge file created")

def git_commit_doc(f, is_n):
    git = sh.git
    sh.cd("../data")
    
    git.add('nodes/')
    git.add('edges/')
    if is_n:
        git.commit(m='add file: '+f)
    else:
        git.commit(m='add edge: '+f)
    print("V:changes commited to git")
